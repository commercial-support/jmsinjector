/**
 * 
 */
package com.telkomsigma.eclears.jmsinjector.jmsinjector.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">Gusde Gita</a>
 * @version $Id: $
 */
@RestController
@RequestMapping("/jmsinjector")
public class JmsInjectorController {

    @Autowired
    JmsTemplate jmsTemplate;

    @Value("${csv.file.folder}")
    private String dir;
    
    @Autowired
    ConnectionFactory connectionFactory;

    @RequestMapping(value = "/injectToQueue", method = RequestMethod.POST)
    public void injectToJms(@RequestParam("filename") String filename, 
	    @RequestParam("corelationId") final String corelationId,
	    @RequestParam("queueDestination") String queueDestination) throws JMSException {

	try (BufferedReader br = new BufferedReader(new FileReader(dir.concat(filename)))) {

	    String sCurrentLine;
	    while ((sCurrentLine = br.readLine()) != null) {	
		
		final String currMsg = sCurrentLine; 
			
		jmsTemplate.send(queueDestination, new MessageCreator() {
		    
		    @Override
		    public Message createMessage(Session session) throws JMSException {
			Message msg = session.createTextMessage(currMsg);
			msg.setJMSCorrelationID(corelationId);
			return msg;
		    }
		});
		System.out.println(sCurrentLine);
		
		try {
		    Thread.sleep(500);
		}
		catch (Exception e) {
		}
		
	    }

	}
	catch (IOException e) {
	    e.printStackTrace();
	}
    }
}
