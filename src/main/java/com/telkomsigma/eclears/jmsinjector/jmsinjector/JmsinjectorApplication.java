package com.telkomsigma.eclears.jmsinjector.jmsinjector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JmsinjectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(JmsinjectorApplication.class, args);
	}
}
